"""Constants for the Smartfox integration."""

# Power 
PV_POWER = "pv"
CAR_CHARGER_POWER = "carCharger"
POWER = "power"
EFFECTIVE_POWER = "effectivePower"
HEAT_PUMP_POWER = "heatPumpPower"
HEAT_PUMP_THERM_POWER = "heatPumpThermPower"
BATTERY_POWER = "batteryPower"

#Energy
ENERGY = "energy"
RETURN_ENERGY = "returnEnergy"
EFFECTIVE_ENERGY = "effectiveEnergy"
APPARENT_ENERGY = "apparentEnergy"

DAY_ENERGY = "dayEnergy"
DAY_RETURN_ENERGY = "dayReturnEnergy"
DAY_EFFECTIVE_ENERGY = "dayEffectiveEnergy"

CAR_CHARGER_CURRENT_CHARGE_ENERGY = "carChargerCurrentChargeEnergy"
CAR_CHARGER_ENERGY = "carChargerEnergy"

HEAT_PUMP_ENERGY = "heatPumpEnergy"
HEAT_PUMP_THERM_ENERGY = "heatPumpThermEnergy"

# Phases
PHASE_1_CURRENT = "phase1Current"
PHASE_1_VOLTAGE = "phase1Voltage"
PHASE_1_POWER = "phase1Power"
PHASE_1_POWER_FACTOR = "phase1PowerFactor"

PHASE_2_CURRENT = "phase2Current"
PHASE_2_VOLTAGE = "phase2Voltage"
PHASE_2_POWER = "phase2Power"
PHASE_2_POWER_FACTOR = "phase2PowerFactor"

PHASE_3_CURRENT = "phase3Current"
PHASE_3_VOLTAGE = "phase3Voltage"
PHASE_3_POWER = "phase3Power"
PHASE_3_POWER_FACTOR = "phase3PowerFactor"

# Relays
RELAY_1_STATE = "relay1State"
RELAY_1_REMAINING_TIME = "relay1RemainingTime"
RELAY_1_OVERALL_TIME = "relay1OverallTime"

RELAY_2_STATE = "relay2State"
RELAY_2_REMAINING_TIME = "relay2RemainingTime"
RELAY_2_OVERALL_TIME = "relay2OverallTime"

RELAY_3_STATE = "relay3State"
RELAY_3_REMAINING_TIME = "relay3RemainingTime"
RELAY_3_OVERALL_TIME = "relay3OverallTime"

RELAY_4_STATE = "relay4State"
RELAY_4_REMAINING_TIME = "relay4RemainingTime"
RELAY_4_OVERALL_TIME = "relay4OverallTime"

# Analog
ANALOG_OUT_PERCENTAGE = "analogOutPercentage"
ANALOG_OUT_POWER = "analogOutPower"
ANALOG_OUT_STATE = "analogOutState"
ANALOG_OUT_MODE = "analogOutMode"


# Battery
BATTERY_SOC = "batterySoc"

# Temperature
BUFFER_HOT = "bufferHot"
BUFFER_COLD = "bufferCold"
WARM_WATER = "warmWater"